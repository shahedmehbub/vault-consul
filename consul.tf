resource "google_compute_instance" "consul" {
  count        = "${var.consul_instances}"
  name         = "${var.name}-consul-${count.index+1}"
  zone         = "${var.zone}"
  machine_type = "${var.machine_type}"

  boot_disk {
    initialize_params {
      image = "${var.source_image}"
    }
  }

  network_interface {
    subnetwork    = "${google_compute_subnetwork.subnet.self_link}"
    access_config = {}
  }

  connection {
    user        = "jcampbell"
    type        = "ssh"
    private_key = "${file(var.ssh_key)}"
    timeout     = "3m"
  }

  provisioner "file" {
    source      = "/Users/jcampbell/.ssh/vault-build-josh-de564b443554.json"
    destination = "/tmp/gcloud.json"
  }

  provisioner "file" {
    source      = "consulserver.sh"
    destination = "/tmp/consulserver.sh"
  }

  provisioner "remote-exec" {
    inline = [
      "export PATH=$PATH:/usr/bin",
      "sudo apt-get install unzip",
      "gcloud auth activate-service-account --key-file=/tmp/gcloud.json",
      "gsutil cp gs://vault-build-josh-bin/bin/consul-enterprise_1.4.4+prem_linux_amd64.zip /tmp/consul.zip",
      "sudo chmod +x /tmp/consulserver.sh",
      "sudo /tmp/./consulserver.sh",
    ]
  }
}
