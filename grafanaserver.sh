#! /bin/bash

wget https://dl.grafana.com/oss/release/grafana-6.2.1.linux-amd64.tar.gz 
tar -zxvf grafana-6.2.1.linux-amd64.tar.gz

sudo mkdir /etc/grafana

sudo mv /home/jcampbell/grafana*/* /etc/grafana/

cat > "/etc/systemd/system/grafana.service" <<EOF
[Unit]
Description=Grafana
Wants=network-online.target
After=network-online.target

[Service]
User=$(whoami)
Group=$(whoami)
Type=simple
ExecStart=/etc/grafana/bin/grafana-server -homepath /etc/grafana

[Install]
WantedBy=multi-user.target
EOF

sudo systemctl enable grafana
sudo systemctl start grafana
sudo systemctl status --no-pager grafana
